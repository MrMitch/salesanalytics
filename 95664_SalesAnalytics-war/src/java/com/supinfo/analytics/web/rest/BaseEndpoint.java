package com.supinfo.analytics.web.rest;

import com.supinfo.analytics.services.DataCrawlerService;
import com.supinfo.analytics.services.SaleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mickael
 */
public abstract class BaseEndpoint 
{
    @EJB
    protected SaleService saleService;
    
    @EJB
    protected DataCrawlerService dataCrawlerService;
    
    protected Map<String, List<String>> parseFilters(List<String> agencies, List<String> genders, 
            List<String> incomeLevels, List<String> maritalStatuses)
    {
        HashMap<String, List<String>> filters = new HashMap<String, List<String>>();
        
        if(agencies.size() > 0 && !agencies.contains("")){
            filters.put("agencies", agencies);
        }
        
        if(maritalStatuses.size() > 0 && !maritalStatuses.contains("")){
            filters.put("maritalStatuses", maritalStatuses);
        }
        
        if(incomeLevels.size() > 0 && !incomeLevels.contains("")){
            filters.put("incomeLevels", incomeLevels);
        }
        
        if(genders.size() > 0 && !genders.contains("")){
            filters.put("genders", genders);
        }
        
        return filters;
    }
    
    protected void crawlIfNoSale()
    {
        if(saleService.getSalesCount(null) == 0)
        {
            dataCrawlerService.crawl();
        }
    }
    
    @GET @Path("/filter") @Produces(MediaType.APPLICATION_JSON)
    public abstract Map<String, Object> filter(
            @QueryParam("agencies") List<String> agencies,
            @QueryParam("genders") List<String> genders,
            @QueryParam("income-levels") List<String> incomeLevels,
            @QueryParam("marital-statuses") List<String> maritalStatuses
            );
    
}
