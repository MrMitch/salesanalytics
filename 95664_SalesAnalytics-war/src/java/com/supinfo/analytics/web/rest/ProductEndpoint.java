package com.supinfo.analytics.web.rest;

import com.supinfo.analytics.services.ProductService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mickael
 */
@Stateless @Path("/products")
public class ProductEndpoint extends BaseEndpoint {
    
    @EJB
    private ProductService productService;
    
    @Override
    @GET @Path("/filter") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> filter(
            @QueryParam("agencies") List<String> agencies,
            @QueryParam("genders") List<String> genders,
            @QueryParam("income-levels") List<String> incomeLevels,
            @QueryParam("marital-statuses") List<String> maritalStatuses
            )
    {
        crawlIfNoSale();
        
        Map<String, List<String>> filters = this.parseFilters(agencies, genders, incomeLevels, maritalStatuses);
        
        HashMap<String, Object> results = new HashMap<String, Object>();
        results.put("bestProducts", productService.getBestSellingProducts(filters));
        results.put("worstProducts", productService.getWorstSellingProducts(filters));
        //results.put("error", "");
        
        return results;
    }
}
