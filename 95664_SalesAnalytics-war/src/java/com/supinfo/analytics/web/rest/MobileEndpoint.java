package com.supinfo.analytics.web.rest;

import com.supinfo.analytics.entities.User;
import com.supinfo.analytics.services.CustomerService;
import com.supinfo.analytics.services.SaleService;
import com.supinfo.analytics.services.UserService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mickael
 */
@Stateless @Path("/mobile")
public class MobileEndpoint extends BaseEndpoint
{
    //@EJB
    //private ChannelService channelService;
    
    @EJB
    private ChannelEndpoint channelEndpoint;
    
    @EJB
    private CustomerEndpoint customerEndpoint;
    
    @EJB
    private ProductEndpoint productEndpoint;
    
    @EJB
    private SaleEndpoint saleEndpoint;
    
    @EJB
    private CustomerService customerService;
    
    @EJB
    private SaleService saleService;
    
    @EJB
    private UserService userService;
    
    @GET @Path("/login") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> login(
            @QueryParam("e") String email, 
            @QueryParam("p") String password)
    {
        User u = userService.authenticateUser(email, password);
        if(u != null)
        {
            HashMap<String, Object> results = new HashMap<String, Object>();
            results.put("token", u.getToken());
            results.put("id", u.getId());
            
            return results;
        }
        
        return null;
    }
    
    @GET @Path("/available-filters") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> getFilters()
    {
        HashMap<String, Object> filters = new HashMap<String, Object>();
        filters.put("agencies", saleService.getAgencies());
        filters.put("genders", customerService.getGenders());
        filters.put("marital-statuses", customerService.getMaritalStatus());
        filters.put("income-levels", customerService.getIncomeLevels());
        
        return filters;
    }

    @Override @GET @Path("/filter") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> filter(
            @QueryParam("agencies") List<String> agencies,
            @QueryParam("genders") List<String> genders,
            @QueryParam("income-levels") List<String> incomeLevels,
            @QueryParam("marital-statuses") List<String> maritalStatuses
            )
    {
        HashMap<String, Object> results = new HashMap<String, Object>();
        Map<String, Object> channels = this.channelEndpoint.filter(agencies, genders, incomeLevels, maritalStatuses);
        channels.put("all", ((Map<String, Object>)channels.get("breakdown")).keySet());
        results.put("channels", channels);
        
        results.put("customers", this.customerEndpoint.filter(agencies, genders, incomeLevels, maritalStatuses));
        results.put("products", this.productEndpoint.filter(agencies, genders, incomeLevels, maritalStatuses));
        
        Map<String, Object> sales = this.saleEndpoint.filter(agencies, genders, incomeLevels, maritalStatuses); 
        results.put("sales", sales);
        results.put("agencies", ((Map<String, Object>)sales.get("agencyBreakdown")).keySet());
        
        return results;
    }
}
