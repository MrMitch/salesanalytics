package com.supinfo.analytics.web.rest;

import com.supinfo.analytics.services.SaleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mickael
 */
@Stateless @Path("/sales")
public class SaleEndpoint extends BaseEndpoint {
    
    @EJB
    private SaleService saleService;
    

    
    @GET @Path("/agencies") @Produces(MediaType.APPLICATION_JSON)
    public List<String> getAllAgencies()
    {
        return saleService.getAgencies();
    }
    
    @Override
    @GET @Path("/filter") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> filter(
            @QueryParam("agencies") List<String> agencies,
            @QueryParam("genders") List<String> genders,
            @QueryParam("income-levels") List<String> incomeLevels,
            @QueryParam("marital-statuses") List<String> maritalStatuses
            )
    {
        crawlIfNoSale();
        
        Map<String, List<String>> filters = this.parseFilters(agencies, genders, incomeLevels, maritalStatuses);
        
        HashMap<String, Object> results = new HashMap<String, Object>();
        results.put("amount", saleService.getTotalAmount(filters));
        results.put("total", saleService.getSalesCount(filters));
        results.put("agencyBreakdown", saleService.getAgenciesRepartition(filters));
        //results.put("error", "");
        
        return results;
    }
    
}
