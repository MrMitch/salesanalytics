package com.supinfo.analytics.web.rest;

import com.supinfo.analytics.services.CustomerService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mickael
 */
@Stateless @Path("/customers")
public class CustomerEndpoint extends BaseEndpoint
{
    @EJB
    private CustomerService customerService;
    
    
    @GET @Path("/income-levels") @Produces(MediaType.APPLICATION_JSON)
    public List<String> getIncomeLevels()
    {
        return customerService.getIncomeLevels();
    }
    
    @GET @Path("/marital-statuses") @Produces(MediaType.APPLICATION_JSON)
    public List<String> getMaritalStatus()
    {
        return customerService.getMaritalStatus();
    }
    
    @GET @Path("/genders") @Produces(MediaType.APPLICATION_JSON)
    public List<String> getGenders()
    {
        return customerService.getGenders();
    }

    @Override
    @GET @Path("/filter") @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> filter(
            @QueryParam("agencies") List<String> agencies,
            @QueryParam("genders") List<String> genders,
            @QueryParam("income-levels") List<String> incomeLevels,
            @QueryParam("marital-statuses") List<String> maritalStatuses
            ) 
    {
        crawlIfNoSale();
        
        Map<String, List<String>> filters = this.parseFilters(agencies, genders, incomeLevels, maritalStatuses);
        HashMap<String, Object> results = new HashMap<String, Object>();
        results.put("gender-breakdown", customerService.getGendersRepartition(filters));
        results.put("income-level-breakdown", customerService.getIncomeLevelRepartition(filters));
        results.put("marital-status-breakdown", customerService.getMaritalStatusRepartition(filters));
        results.put("bestCustomers", customerService.getBestCustomers(filters));
        //results.put("error", "");
        
        return results;
    }
    
    
}
