package com.supinfo.analytics.web.beans;

import com.supinfo.analytics.entities.User;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author mickael
 */
@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private User loggedUser;
    

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }
    
    public boolean isSomeoneLogged()
    {
        return this.loggedUser != null;
    }
}
