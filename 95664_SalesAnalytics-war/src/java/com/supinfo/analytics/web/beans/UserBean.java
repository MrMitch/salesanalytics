package com.supinfo.analytics.web.beans;

import com.supinfo.analytics.entities.User;
import com.supinfo.analytics.services.UserService;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author mickael
 */
@ManagedBean
@ViewScoped
public class UserBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @EJB
    private UserService userService;
    
    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;
    
    private String email;
    private String password;
    
    public String login() {
        User userTmp = userService.authenticateUser(email, password);
        String result = null;
        
        if(userTmp != null) {
            
            this.loginBean.setLoggedUser(userTmp);
            result = "/dashboard?faces-redirect=true";
        }
        else 
        {
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, 
                    "Please check your credentials", 
                    "Please check your credentials");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        return result;
    }
    
    public String logout() {
        loginBean.setLoggedUser(null);
        
        return "/login?faces-redirect=true";
    }
    
    // GETTERS AND SETTERS

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}