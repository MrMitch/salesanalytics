package com.supinfo.analytics.web.filters;

import com.supinfo.analytics.web.beans.LoginBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mickael
 */
@WebFilter(urlPatterns="/dashboard.xhtml")
public class DashboardFilter implements Filter 
{
    private LoginBean loginBean;

    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        
        loginBean = (LoginBean) req.getSession().getAttribute("loginBean");
        
        if(loginBean == null || loginBean.getLoggedUser() == null) 
        {
            resp.sendRedirect(request.getServletContext().getContextPath() + "/login.xhtml");
        }
        else 
        {
            chain.doFilter(request, response);
        }
    }

    
    
    /**
     * OTHER METHODS
     */
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
        
    }    
}
