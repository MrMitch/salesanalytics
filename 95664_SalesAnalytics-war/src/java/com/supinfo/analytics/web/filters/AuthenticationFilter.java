package com.supinfo.analytics.web.filters;

import com.supinfo.analytics.services.UserService;
import com.supinfo.analytics.web.beans.LoginBean;
import com.supinfo.analytics.web.beans.UserBean;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mickael
 */
@WebFilter(urlPatterns="/app/*")
public class AuthenticationFilter implements Filter {

    private LoginBean loginBean;
    private UserBean userBean;
    
    @EJB
    private UserService userService;
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        loginBean = (LoginBean) httpRequest.getSession().getAttribute("loginBean");
        //userBean = (UserBean) httpRequest.getSession().getAttribute("userBean");
        
        if(httpRequest.getRequestURL().toString().endsWith("/app/rest/mobile/login"))
        {
            chain.doFilter(request, response);
        }
        else if((loginBean == null || loginBean.getLoggedUser() == null)) 
        {
            /**
             * Login authentication with token & id
             */
            String token = request.getParameter("token");
            String idParam = request.getParameter("id");
            
            if(idParam == null || token == null)
            {
                httpResponse.sendRedirect(request.getServletContext().getContextPath() + "/error.json");
            }
            else
            {
                Long id = Long.valueOf(idParam);
                
                System.out.println("---token + id");
                System.out.println(token);
                System.out.println(id);
                System.out.println("---token + id");
                
                if(!userService.validateToken(token, id))
                {
                    httpResponse.sendRedirect(request.getServletContext().getContextPath() + "/error.json");
                }
                else
                {
                    chain.doFilter(request, response);
                }
            }
        } 
        else 
        {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void destroy() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
