#!/bin/bash
SALT="JAVA KAKA OMFG WTF BBQ ↑↑↓↓←→←→BA"
echo "This script will generate the hash for your password and a random token for your user that will be used by the Android app."
echo "Enter your password: "
read -s RAW
echo "hash: "
HASH=`echo -n $RAW$SALT | md5sum | tr -d " -"`
echo $HASH
echo "token: "
< /dev/urandom tr -dc _A-Z-a-z-0-9\{\}\*%\;$ | head -c${1:-64}; echo;
