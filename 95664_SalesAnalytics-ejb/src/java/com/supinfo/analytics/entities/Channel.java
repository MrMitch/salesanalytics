package com.supinfo.analytics.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author mickael
 */
@Entity
public class Channel implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer id;
    
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
