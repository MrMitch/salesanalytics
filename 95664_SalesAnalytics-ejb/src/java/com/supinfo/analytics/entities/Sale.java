package com.supinfo.analytics.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author mickael
 */
@Entity
public class Sale implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer id;

    private BigDecimal quantitySold;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date time;
    
    private BigDecimal amountSold;
    
    @ManyToOne
    private Channel channel;
    
    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Product product;

    private String agency;
    
    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public BigDecimal getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(BigDecimal quantitySold) {
        this.quantitySold = quantitySold;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    public BigDecimal getAmountSold() {
        return amountSold;
    }

    public void setAmountSold(BigDecimal amountSold) {
        this.amountSold = amountSold;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
