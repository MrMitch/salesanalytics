package com.supinfo.analytics.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author mickael
 */
@Entity
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer id;

    private String name;
    
    // this attribute doesn't need to be persisted, 
    // it's dynamically computed based on the filters that are provided 
    // in the dashboard, its only purpose is informational
    @Transient
    private Object quantitySold;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(Object quantitySold) {
        this.quantitySold = quantitySold;
    }
}
