package com.supinfo.analytics.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 *
 * @author mickael
 */
@Entity
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer id;
    
    private String firstName;
    
    private String lastName;
    
    private String gender;
    
    private String maritalStatus;
    
    private String incomeLevel;
    
    private String city;
    
    // this attribute doesn't need to be persisted, 
    // it's dynamically computed based on the filters that are provided 
    // in the dashboard, its only purpose is informational
    @Transient
    private Object salesAmount;

    public Object getSalesAmount() {
        return salesAmount;
    }

    public void setSalesAmount(Object salesAmount) {
        this.salesAmount = salesAmount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        if(gender != null && !gender.equals(""))
        {
            this.gender = gender.toUpperCase().substring(0, 1);
        }
        else
        {
            this.gender = "other";
        }
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        if(maritalStatus != null && !maritalStatus.equals("") && !maritalStatus.equals("null"))
        {
            this.maritalStatus = maritalStatus;
        }
        else
        {
            this.maritalStatus = "unknown";
        }
    }

    public String getIncomeLevel() {
        return incomeLevel;
    }

    public void setIncomeLevel(String incomeLevel) {
        this.incomeLevel = incomeLevel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
