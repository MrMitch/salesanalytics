package com.supinfo.analytics.dao;

import com.supinfo.analytics.entities.Channel;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
/**
 *
 * @author mickael
 */
@Local
public interface ChannelDAO {
    
    public Channel addChannel(Channel c);
    
    public List<Channel> getAllChannels();
    
    public Map<String, Object> getChannelsRepartition(Map<String, List<String>> filters); 
    
}
