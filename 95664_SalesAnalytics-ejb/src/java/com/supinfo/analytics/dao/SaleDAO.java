package com.supinfo.analytics.dao;

import com.supinfo.analytics.entities.Sale;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author mickael
 */
@Local
public interface SaleDAO {
    
    public Sale addSale(Sale s);
    
    public Integer getSalesNumber(Map<String, List<String>> filters);
    
    public BigDecimal getTotalAmountSold(Map<String, List<String>> filters);
   
    public List<String> getAllAgencies();
    
    public Map<String, Object> getAgenciesRepartitions(Map<String, List<String>> filters);
}
