package com.supinfo.analytics.dao;

import com.supinfo.analytics.entities.Customer;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author mickael
 */
@Local
public interface CustomerDAO {
    public Customer addCustomer(Customer c);
    
    public List<String> getAllIncomeLevels();
    
    public List<String> getAllMaritalStatus();
    
    public List<String> getAllGenders();
    
    public Map<String, Object> getGendersRepartition(Map<String, List<String>> filters);
   
    public Map<String, Object> getIncomeLevelRepartition(Map<String, List<String>> filters);
    
    public Map<String, Object> getMaritalStatusRepartition(Map<String, List<String>> filters);
    
    public List<Customer> getBestCustomers(Map<String, List<String>> filters);
}
