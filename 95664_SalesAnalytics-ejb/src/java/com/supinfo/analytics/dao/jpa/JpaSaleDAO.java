package com.supinfo.analytics.dao.jpa;

import com.supinfo.analytics.dao.SaleDAO;
import com.supinfo.analytics.entities.Sale;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
@Stateless
public class JpaSaleDAO extends BaseJpaDAO implements SaleDAO {

    @PersistenceContext(unitName="SalesAnalytics-PU")
    private EntityManager em;
    
    @Override
    public Sale addSale(Sale s) {
        em.merge(s);
        
        return s;
    }

    @Override
    public Integer getSalesNumber(Map<String, List<String>> filters) {
        String req = "SELECT COUNT(id) as totalSale FROM Sale s";
        req = aggregateFilters(req, filters);
        Query q = em.createQuery(req);
        this.setParametersFromFilters(q, filters);
        
        Integer count = Integer.parseInt(q.getSingleResult().toString());
        
        return count;
    }

    @Override
    public BigDecimal getTotalAmountSold(Map<String, List<String>> filters) {
        
        String req = aggregateFilters("SELECT SUM(s.amountSold) FROM Sale s", filters);
        
        Query q = em.createQuery(req);
        this.setParametersFromFilters(q, filters);
        
        Object count = q.getSingleResult();
        
        return (BigDecimal) count;
    }

    @Override
    public List<String> getAllAgencies() {
        Query q = em.createQuery("SELECT DISTINCT(agency) FROM Sale");
        
        return q.getResultList();
    }

    @Override
    public Map<String, Object> getAgenciesRepartitions(Map<String, List<String>> filters) {
        
        String req = "SELECT s.agency, SUM(s.amountSold) as amount FROM Sale s";
        req = this.aggregateFilters(req, filters) + " GROUP BY s.agency ORDER BY amount";
        Query q = em.createQuery(req);
        
        this.setParametersFromFilters(q, filters);
        
        List<Object[]> results = q.getResultList();
        return this.buildBreakdownFromResultList(results);
    }
    
    
    
}
