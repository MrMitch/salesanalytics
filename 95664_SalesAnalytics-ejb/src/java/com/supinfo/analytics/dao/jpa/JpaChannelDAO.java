package com.supinfo.analytics.dao.jpa;

import com.supinfo.analytics.dao.ChannelDAO;
import com.supinfo.analytics.entities.Channel;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
@Stateless
public class JpaChannelDAO extends BaseJpaDAO implements ChannelDAO {

    @PersistenceContext(unitName="SalesAnalytics-PU")
    private EntityManager em;
    
    @Override
    public Channel addChannel(Channel c) {
        em.merge(c);
        
        return c;
    }
    
    @Override
    public List<Channel> getAllChannels()
    {
        String req = "SELECT c FROM Channel c";
        Query q = em.createQuery(req);
        
        return q.getResultList();
    }

    @Override
    public Map<String, Object> getChannelsRepartition(Map<String, List<String>> filters) {
        String req = "SELECT s.channel.description, SUM(s.amountSold) as amount FROM Sale s";
        req = this.aggregateFilters(req, filters) + " GROUP BY s.channel ORDER BY amount";
        Query q = em.createQuery(req);
        
        this.setParametersFromFilters(q, filters);
        
        List<Object[]> results = q.getResultList();
        return this.buildBreakdownFromResultList(results);
    }
    
}
