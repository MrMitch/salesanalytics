package com.supinfo.analytics.dao.jpa;

import com.supinfo.analytics.dao.UserDAO;
import com.supinfo.analytics.entities.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
@Stateless
public class JpaUserDAO extends BaseJpaDAO implements UserDAO{

    @PersistenceContext(unitName="SalesAnalytics-PU")
    private EntityManager em;
    
    @Override
    public User authenticateUser(String email, String password) {
        try{
            String req = "SELECT u FROM User u WHERE u.email = :email AND u.password = :password";
            Query q = em.createQuery(req);
            q.setParameter("email", email);
            q.setParameter("password", password);

            return (User) q.getSingleResult();
        }
        catch(NoResultException e)
        {
            return null;
        }
    }
    
    @Override
    public User authenticateUser(String token, Long id) {
        try {
            String req = "SELECT u FROM User u WHERE u.id = :id AND u.token = :token ";
            Query q = em.createQuery(req);
            q.setParameter("token", token);
            q.setParameter("id", id);

            return (User) q.getSingleResult();
        }
        catch(NonUniqueResultException nu)
        {
            return null;
        }
        catch(NoResultException e)
        {
            return null;
        }
    }
    
}
