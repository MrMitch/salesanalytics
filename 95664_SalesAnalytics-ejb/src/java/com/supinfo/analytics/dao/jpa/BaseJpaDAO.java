package com.supinfo.analytics.dao.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
public abstract class BaseJpaDAO 
{
    protected String aggregateFilters(String req, Map<String, List<String>> filters)
    {
        Boolean and = Boolean.FALSE;
        if(filters != null && filters.size() > 0)
        {
            req += " WHERE";
            
            if(filters.containsKey("agencies"))
            {
                req += (and ? " AND" : "") + " s.agency IN :agencies";
                
                if(!and)
                {
                    and = Boolean.TRUE;
                }
            }
            
            if(filters.containsKey("genders"))
            {
                req += (and ? " AND" : "") + " s.customer.gender IN :genders";
                
                if(!and)
                {
                    and = Boolean.TRUE;
                }
            }
            
            if(filters.containsKey("maritalStatuses"))
            {
                req += (and ? " AND" : "") + " s.customer.maritalStatus IN :maritalStatuses";
                
                if(!and)
                {
                    and = Boolean.TRUE;
                }
            }
            
            if(filters.containsKey("incomeLevels"))
            {
                req += (and ? " AND" : "") + " s.customer.incomeLevel IN :incomeLevels";
            }
        }
        
        return req;
    }
    
    protected Query setParametersFromFilters(Query q, Map<String, List<String>> filters)
    {
        if(filters != null)
        {
            for(Map.Entry<String, List<String>> filter : filters.entrySet())
            {
                q.setParameter(filter.getKey(), filter.getValue());
            }
        }
        
        return q;
    }
    
    
    protected Map<String, Object> buildBreakdownFromResultList(List<Object[]> results)
    {
        HashMap<String, Object> breakdown = new HashMap<String, Object>();
        for(Object[] result : results)
        {
            breakdown.put(result[0].toString(), result[1]);
        }
        
        return breakdown;
    }
}
