package com.supinfo.analytics.dao.jpa;

import com.supinfo.analytics.dao.ProductDAO;
import com.supinfo.analytics.entities.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
@Stateless
public class JpaProductDAO extends BaseJpaDAO implements ProductDAO {

    @PersistenceContext(unitName="SalesAnalytics-PU")
    private EntityManager em;
        
    @Override
    public Product addProduct(Product p) {
        em.merge(p);
        
        return p;
    }
    
    @Override
    public List<Product> getWorstSellingProducts(Map<String, List<String>> filters) {
        return getHierarchicalProductsList(filters, "ASC");
    }

    @Override
    public List<Product> getBestSellingProducts(Map<String, List<String>> filters) {
        return getHierarchicalProductsList(filters, "DESC");
    }
    
    protected List<Product> getHierarchicalProductsList(Map<String, List<String>> filters, String order) {
        String req = aggregateFilters("SELECT s.product, SUM(s.quantitySold) as sold from Sale s", filters) 
                + " GROUP BY s.product ORDER BY sold " + order;
        
        Query q = em.createQuery(req);
        this.setParametersFromFilters(q, filters);
        
        List<Object[]> results = q.setMaxResults(10).getResultList();
        List<Product> products = new ArrayList<Product>();
        
        for(Object[] result : results)
        {
            Product p = (Product) result[0];
            p.setQuantitySold(result[1]);
            products.add(p);
        }
        
        return products;
    }
    
}
