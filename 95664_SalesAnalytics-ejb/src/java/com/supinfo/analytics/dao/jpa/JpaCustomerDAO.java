package com.supinfo.analytics.dao.jpa;

import com.supinfo.analytics.dao.CustomerDAO;
import com.supinfo.analytics.entities.Customer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author mickael
 */
@Stateless
public class JpaCustomerDAO extends BaseJpaDAO implements CustomerDAO {

    @PersistenceContext(unitName="SalesAnalytics-PU")
    private EntityManager em;

    @Override
    public Customer addCustomer(Customer c) {
        em.merge(c);
        
        return c;
    }

    @Override
    public List<String> getAllIncomeLevels() {
        return getDistinctValues("incomeLevel");
    }

    @Override
    public List<String> getAllMaritalStatus() {
        return getDistinctValues("maritalStatus");
    }

    @Override
    public List<String> getAllGenders() {
        return getDistinctValues("gender");
    }
    
    
    
    protected List<String> getDistinctValues(String name)
    {
        String req = "SELECT DISTINCT(%s) FROM Customer GROUP BY %s ORDER BY %s";
        Query q = em.createQuery(String.format(req, name, name, name));
        List<String> levels = q.getResultList();
        
        return levels;
    }

    protected Map<String, Object> getBreakdown(String field, Map<String, List<String>> filters)
    {
        String req = "SELECT %s, COUNT(s) as amount FROM Sale s";
        req = this.aggregateFilters(req, filters) + " GROUP BY %s ORDER BY amount";
        
        req = String.format(req, field, field);
        
        Query q = em.createQuery(req);
        
        this.setParametersFromFilters(q, filters);
        
        List<Object[]> results = q.getResultList();
        return this.buildBreakdownFromResultList(results);
    }
    
    @Override
    public Map<String, Object> getGendersRepartition(Map<String, List<String>> filters) 
    {
        return this.getBreakdown("s.customer.gender", filters);
    }

    @Override
    public Map<String, Object> getIncomeLevelRepartition(Map<String, List<String>> filters) 
    {
        return this.getBreakdown("s.customer.incomeLevel", filters);
    }

    @Override
    public Map<String, Object> getMaritalStatusRepartition(Map<String, List<String>> filters) 
    {
        return this.getBreakdown("s.customer.maritalStatus", filters);
    }

    @Override
    public List<Customer> getBestCustomers(Map<String, List<String>> filters) 
    {
        String req = "SELECT s.customer, COUNT(s) as salesCount FROM Sale s";
        req = this.aggregateFilters(req, filters) + " GROUP BY s.customer ORDER BY salesCount DESC";
        
        Query q = em.createQuery(req);
        this.setParametersFromFilters(q, filters);
        List<Object[]> results = q.setMaxResults(10).getResultList(); 
        List<Customer> customers = new ArrayList<Customer>();
        
        for(Object[] result : results)
        {
            Customer c = (Customer) result[0];
            c.setSalesAmount(result[1]);
            customers.add(c);
        }
        
        return customers;
    }
    
}
