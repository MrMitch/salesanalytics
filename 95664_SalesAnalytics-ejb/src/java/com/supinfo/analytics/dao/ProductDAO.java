package com.supinfo.analytics.dao;

import com.supinfo.analytics.entities.Product;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author mickael
 */
@Local
public interface ProductDAO {
    
    public Product addProduct(Product p);
    
    public List<Product> getWorstSellingProducts(Map<String, List<String>> filters);
    
    public List<Product> getBestSellingProducts(Map<String, List<String>> filters);
    
}
