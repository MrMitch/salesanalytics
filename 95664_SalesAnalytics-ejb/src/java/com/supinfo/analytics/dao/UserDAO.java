package com.supinfo.analytics.dao;

import com.supinfo.analytics.entities.User;
import javax.ejb.Local;

/**
 *
 * @author mickael
 */
@Local
public interface UserDAO {
    
    public User authenticateUser(String email, String password);
    public User authenticateUser(String token, Long id);
    
}
