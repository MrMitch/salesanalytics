package com.supinfo.analytics.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Stateless;

/**
 * Utility class to hash a password using a salt
 * @author mickael
 */
@Stateless
public class PasswordHasher {
    private static String salt = "JAVA KAKA OMFG WTF BBQ ↑↑↓↓←→←→BA";
    
    public String hash(String password) {
        String sum = password.concat(PasswordHasher.salt);
        
        byte[] uniqueKey = sum.getBytes();
        byte[] hash = null;

        try 
        {
            hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }

        StringBuilder hashString = new StringBuilder();
        for (int i = 0; i < hash.length; i++)
        {
            String hex = Integer.toHexString(hash[i]);
            if (hex.length() == 1)
            {
                hashString.append('0');
                hashString.append(hex.charAt(hex.length() - 1));
            }
            else
            {
                hashString.append(hex.substring(hex.length() - 2));
            }
        }
        
        return hashString.toString();
    }
}
