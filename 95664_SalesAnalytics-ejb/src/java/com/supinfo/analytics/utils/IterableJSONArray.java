package com.supinfo.analytics.utils;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Custom iterable wrapper for JSONArray populated with JSONObjects.
 * @author mickael
 */
public class IterableJSONArray implements Iterable<JSONObject> {

    protected JSONArray original;
    
    
    public IterableJSONArray(JSONArray jsonArray) {
        this.original = jsonArray;
    }

    
    @Override
    public Iterator<JSONObject> iterator() {
        return new Iterator<JSONObject>() {
            
            protected int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return this.currentIndex < original.length();
            }

            @Override
            public JSONObject next() {
                return original.optJSONObject(this.currentIndex++);
            }

            @Override
            public void remove() {
                //throw new UnsupportedOperationException("Not supported yet.");
            }

        };
        
    }
}
