package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.ChannelDAO;
import com.supinfo.analytics.entities.Channel;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class ChannelService {
    
    @EJB
    private ChannelDAO channelDAO;
    
    public List<Channel> getChannels()
    {
        return channelDAO.getAllChannels();
    }
    
    public Map<String, Object> getChannelsRepartition(Map<String, List<String>> filters)
    {
        return channelDAO.getChannelsRepartition(filters);
    }
}
