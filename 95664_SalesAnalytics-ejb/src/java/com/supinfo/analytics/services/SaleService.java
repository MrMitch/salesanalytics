package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.SaleDAO;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class SaleService {
    
    @EJB
    private SaleDAO saleDAO;
    
    public Integer getSalesCount(Map<String, List<String>> filters) {
        return saleDAO.getSalesNumber(filters);
    }
    
    public BigDecimal getTotalAmount(Map<String, List<String>> filters){
        return saleDAO.getTotalAmountSold(filters);
    }
    
    public List<String> getAgencies()
    {
        return saleDAO.getAllAgencies();
    }
    
    public Map<String, Object> getAgenciesRepartition(Map<String, List<String>> filters)
    {
        return saleDAO.getAgenciesRepartitions(filters);
    }
}
