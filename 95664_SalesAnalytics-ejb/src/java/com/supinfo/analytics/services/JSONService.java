package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.ChannelDAO;
import com.supinfo.analytics.dao.CustomerDAO;
import com.supinfo.analytics.dao.ProductDAO;
import com.supinfo.analytics.dao.SaleDAO;
import com.supinfo.analytics.entities.Channel;
import com.supinfo.analytics.entities.Customer;
import com.supinfo.analytics.entities.Product;
import com.supinfo.analytics.entities.Sale;
import com.supinfo.analytics.utils.IterableJSONArray;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author mickael
 */
@Stateless
public class JSONService {
    
    @EJB
    private SaleDAO saleDAO;
    
    @EJB
    private CustomerDAO customerDAO;
    
    @EJB
    private ProductDAO productDAO;
    
    @EJB
    private ChannelDAO channelDAO;
    
    public void processJSONList(String url, String agency) throws IOException
    {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        try 
        {
            StringBuilder stringBuilder = new StringBuilder();
            int buffer;
            while ((buffer = reader.read()) != -1) {
                stringBuilder.append((char) buffer);
            }
            
            JSONArray sales = new JSONArray(stringBuilder.toString());
            
            // JSONArray n'est pas iterable avec un foreach de base
            // du coup j'ai fait un iterator perso \o/ (com.supinfo.analytics.utils.IterableJSONArray)
            IterableJSONArray jsonSales = new IterableJSONArray(sales);
            
            for (JSONObject jsonSale : jsonSales) 
            {
                JSONObject jsonChannel = jsonSale.getJSONObject("channel");
                JSONObject jsonProduct = jsonSale.getJSONObject("product");
                JSONObject jsonCustomer = jsonSale.getJSONObject("customer");

                Channel c = new Channel();
                c.setId(jsonChannel.getInt("channelId"));
                c.setDescription(jsonChannel.getString("channelDesc"));
                
                Product p = new Product();
                p.setId(jsonProduct.getInt("prodId"));
                p.setName(jsonProduct.getString("prodName"));
                
                Customer cu = new Customer();
                cu.setCity(jsonCustomer.getString("custCity"));
                cu.setFirstName(jsonCustomer.getString("custFirstName"));
                cu.setGender(jsonCustomer.getString("custGender"));
                cu.setId(jsonCustomer.getInt("custId"));
                cu.setIncomeLevel(jsonCustomer.getString("custIncomeLevel"));
                cu.setLastName(jsonCustomer.getString("custLastName"));
                cu.setMaritalStatus(jsonCustomer.getString("custMaritalStatus"));
                
                channelDAO.addChannel(c);
                productDAO.addProduct(p);
                customerDAO.addCustomer(cu);
                
                Sale s = new Sale();
                s.setAgency(agency);
                s.setAmountSold(BigDecimal.valueOf(jsonSale.getDouble("amountSold")));
                s.setChannel(c);
                s.setCustomer(cu);
                s.setId(jsonSale.getInt("saleId"));
                s.setProduct(p);
                s.setQuantitySold(BigDecimal.valueOf(jsonSale.getInt("quantitySold")));
                
                SimpleDateFormat formatter = new SimpleDateFormat("y-M-d");
                try 
                {
                    s.setTime(formatter.parse(jsonSale.getString("timeId")));
                } 
                catch (ParseException ex) 
                {
                    Logger.getLogger(JSONService.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                saleDAO.addSale(s);
            }
        }
        catch(Exception e)
        {
            Logger.getLogger(JSONService.class.getName()).log(Level.SEVERE, null, e);
        }
        finally 
        {
            inputStream.close();
            reader.close();
        }
    }
}
