package com.supinfo.analytics.services;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class DataCrawlerService 
{
    @EJB
    private JSONService jsonService;
    
    @EJB
    private CSVService csvService;
    
    @EJB
    private SOAPService soapService;
    
    // Automated crawl method, running everyday at 3:00 AM
    @Schedule(hour="3", minute="0", second = "0")
    public void crawl()
    {
        try {
            jsonService.processJSONList("http://supsellermontreal.supinfo.cloudbees.net/sales/export.json", "Montréal");
            csvService.processCSVList("http://supsellertokyo.supinfo.cloudbees.net/sales/export.csv", "Tokyo");
            soapService.processSoapList("Paris");
            
            Logger.getLogger(DataCrawlerService.class.getName()).log(Level.INFO, "Data crawled automagicaly.");
        } catch (IOException ex) {
            Logger.getLogger(DataCrawlerService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
