package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.CustomerDAO;
import com.supinfo.analytics.entities.Customer;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class CustomerService {
    
    @EJB
    private CustomerDAO customerDAO;
    
    public List<String> getIncomeLevels()
    {
        return customerDAO.getAllIncomeLevels();
    }
    
    public List<String> getMaritalStatus()
    {
        return customerDAO.getAllMaritalStatus();
    }
    
    public List<String> getGenders()
    {
        return customerDAO.getAllGenders();
    }
    
    public Map<String, Object> getGendersRepartition(Map<String, List<String>> filters)
    {
        return customerDAO.getGendersRepartition(filters);
    }
    
    public Map<String, Object> getMaritalStatusRepartition(Map<String, List<String>> filters)
    {
        return customerDAO.getMaritalStatusRepartition(filters);
    }
    
    public Map<String, Object> getIncomeLevelRepartition(Map<String, List<String>> filters)
    {
        return customerDAO.getIncomeLevelRepartition(filters);
    }
    
    public List<Customer> getBestCustomers(Map<String, List<String>> filters)
    {
        return customerDAO.getBestCustomers(filters);
    }
}
