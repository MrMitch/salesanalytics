package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.ChannelDAO;
import com.supinfo.analytics.dao.CustomerDAO;
import com.supinfo.analytics.dao.ProductDAO;
import com.supinfo.analytics.dao.SaleDAO;
import com.supinfo.analytics.entities.Channel;
import com.supinfo.analytics.entities.Customer;
import com.supinfo.analytics.entities.Product;
import com.supinfo.analytics.entities.Sale;
import com.supinfo.analytics.soap.SalesExport;
import com.supinfo.analytics.soap.SalesExportService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class SOAPService {
    
    @EJB
    private SaleDAO saleDAO;
    
    @EJB
    private CustomerDAO customerDAO;
    
    @EJB
    private ProductDAO productDAO;
    
    @EJB
    private ChannelDAO channelDAO;
    
    public void processSoapList(String agency)
    {
        SalesExportService service = new SalesExportService();
        SalesExport export = service.getSalesExportPort();
        List<com.supinfo.analytics.soap.Sale> soapSales = export.getFranceSales();
        
        for(com.supinfo.analytics.soap.Sale s : soapSales)
        {
            Channel c = new Channel();
            c.setDescription(s.getChannel().getChannelDesc());
            c.setId(s.getChannel().getChannelId());

            Product p = new Product();
            p.setName(s.getProduct().getProdName());
            p.setId(s.getProduct().getProdId());

            Customer cu = new Customer();
            cu.setCity(s.getCustomer().getCustCity());
            cu.setFirstName(s.getCustomer().getCustFirstName());
            cu.setGender(s.getCustomer().getCustGender());
            cu.setId(s.getCustomer().getCustId());
            cu.setIncomeLevel(s.getCustomer().getCustIncomeLevel());
            cu.setLastName(s.getCustomer().getCustLastName());
            cu.setMaritalStatus(s.getCustomer().getCustMaritalStatus());

            channelDAO.addChannel(c);
            productDAO.addProduct(p);
            customerDAO.addCustomer(cu);

            Sale sale = new com.supinfo.analytics.entities.Sale();
            sale.setAgency(agency);
            sale.setAmountSold(s.getAmountSold());
            sale.setChannel(c);
            sale.setCustomer(cu);
            sale.setId(s.getSaleId());
            sale.setProduct(p);
            sale.setQuantitySold(s.getQuantitySold());

            SimpleDateFormat formatter = new SimpleDateFormat("y-M-d");
            try 
            {
                sale.setTime(formatter.parse(s.getTimeId()));
            } 
            catch (ParseException ex) 
            {
                Logger.getLogger(SOAPService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            saleDAO.addSale(sale);
        }
        
        
    }
    
    
}
