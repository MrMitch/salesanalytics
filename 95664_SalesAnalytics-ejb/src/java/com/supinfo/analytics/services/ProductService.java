package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.ProductDAO;
import com.supinfo.analytics.entities.Product;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class ProductService {
    
    @EJB
    private ProductDAO productDAO;
    
    public List<Product> getBestSellingProducts(Map<String, List<String>> filters)
    {
        return productDAO.getBestSellingProducts(filters);
    }
    
    public List<Product> getWorstSellingProducts(Map<String, List<String>> filters)
    {
        return productDAO.getWorstSellingProducts(filters);
    }
}
