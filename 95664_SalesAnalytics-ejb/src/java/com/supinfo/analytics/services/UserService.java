package com.supinfo.analytics.services;

import com.supinfo.analytics.dao.UserDAO;
import com.supinfo.analytics.entities.User;
import com.supinfo.analytics.utils.PasswordHasher;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class UserService {
    @EJB
    private PasswordHasher passwordHasher;
    
    @EJB
    private UserDAO dao;
    
    public User authenticateUser(String email, String password) {
        return dao.authenticateUser(email, passwordHasher.hash(password));
    }
    
    public boolean validateToken(String token, Long id) {
        return (dao.authenticateUser(token, id) != null);
    }
}
