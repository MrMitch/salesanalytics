package com.supinfo.analytics.services;

import au.com.bytecode.opencsv.CSVReader;
import com.supinfo.analytics.dao.ChannelDAO;
import com.supinfo.analytics.dao.CustomerDAO;
import com.supinfo.analytics.dao.ProductDAO;
import com.supinfo.analytics.dao.SaleDAO;
import com.supinfo.analytics.entities.Channel;
import com.supinfo.analytics.entities.Customer;
import com.supinfo.analytics.entities.Product;
import com.supinfo.analytics.entities.Sale;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author mickael
 */
@Stateless
public class CSVService {
    
    public static final int SALE_TIME = 0;
    public static final int SALE_QUANTITY_SOLD = 1;
    public static final int SALE_AMOUNT_SOLD = 2;
    public static final int SALE_ID = 3;
    
    public static final int PRODUCT_ID = 4;
    public static final int PRODUCT_NAME = 5;
    public static final int PRODUCT_DESCRIPTION = 6;
    public static final int PRODUCT_CATEGORY = 10;
    public static final int PRODUCT_CATEGORY_ID = 11;
    
    public static final int CHANNEL_ID = 60;
    public static final int CHANNEL_DESCRIPTION = 61;
    
    public static final int CUSTOMER_ID = 26;
    public static final int CUSTOMER_FIRSTNAME = 27;
    public static final int CUSTOMER_LASTNAME = 28;
    public static final int CUSTOMER_GENDER = 29;
    public static final int CUSTOMER_MARITAL_STATUS = 31;
    public static final int CUSTOMER_CITY = 34;
    public static final int CUSTOMER_INCOME_LEVEL = 39;
    
    @EJB
    private CustomerDAO customerDAO;
    
    @EJB
    private SaleDAO saleDAO;
    
    @EJB
    private ProductDAO productDAO;
    
    @EJB
    private ChannelDAO channelDAO;
    
    
    public void processCSVList(String url, String agency) throws IOException
    {
        InputStream inputStream = new URL(url).openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
        CSVReader csvReader = new CSVReader(reader);
        
        try 
        {
            // sauter la ligne du header
            csvReader.readNext();
            List<String[]> csvLines = csvReader.readAll();

            for(String[] info : csvLines)
            {
                Channel c = new Channel();
                c.setDescription(info[CHANNEL_DESCRIPTION]);
                c.setId(Integer.parseInt(info[CHANNEL_ID]));

                Product p = new Product();
                p.setId(Integer.parseInt(info[PRODUCT_ID]));
                p.setName(info[PRODUCT_NAME]);

                Customer cu = new Customer();
                cu.setCity(info[CUSTOMER_CITY]);
                cu.setFirstName(info[CUSTOMER_FIRSTNAME]);
                cu.setGender(info[CUSTOMER_GENDER]);
                cu.setId(Integer.valueOf(info[CUSTOMER_ID]));
                cu.setIncomeLevel(info[CUSTOMER_INCOME_LEVEL]);
                cu.setLastName(info[CUSTOMER_LASTNAME]);
                cu.setMaritalStatus(info[CUSTOMER_MARITAL_STATUS]);

                channelDAO.addChannel(c);
                productDAO.addProduct(p);
                customerDAO.addCustomer(cu);

                Sale s = new Sale();
                s.setAgency(agency);
                s.setAmountSold(new BigDecimal(info[SALE_AMOUNT_SOLD]));
                s.setChannel(c);
                s.setCustomer(cu);
                s.setId(Integer.valueOf(info[SALE_ID]));
                s.setProduct(p);
                s.setQuantitySold(new BigDecimal(info[SALE_QUANTITY_SOLD]));

                SimpleDateFormat formatter = new SimpleDateFormat("y-M-d");
                try 
                {
                    s.setTime(formatter.parse(info[SALE_TIME]));
                } 
                catch (ParseException ex) 
                {
                    Logger.getLogger(CSVService.class.getName()).log(Level.SEVERE, null, ex);
                }

                saleDAO.addSale(s);
            }
        } 
        catch (Exception e) 
        {
            Logger.getLogger(CSVService.class.getName()).log(Level.SEVERE, null, e);
        }
        finally
        {
            csvReader.close();
            reader.close();
            inputStream.close();
        }
    }
}
